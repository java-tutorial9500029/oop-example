package com.example.oop;

public class Application {
    public static void main(String[] args) {
        // Polymorphism - using Person reference to refer to Student and Teacher objects
        Person person1 = new Student("Ali Kamal", 20, 101);
        Person person2 = new Teacher("Jamal Ahadi", 35, "Math");

        // Abstraction - calling the displayInfo method without knowing the actual class type
        person1.displayInfo();
        person2.displayInfo();

        // Encapsulation - accessing properties through getters and setters
        person1.setName("Ali Kamal updated");
        person1.setAge(21);
        person1.displayInfo();
    }
}
