package com.example.oop;

class Student extends Person {
    private int rollNumber;

    public Student(String name, int age, int rollNumber) {
        super(name, age);
        this.rollNumber = rollNumber;
    }

    @Override
    public void displayInfo() {
        System.out.println("Student: " + getName() + ", Age: " + getAge() + ", Roll Number: " + rollNumber);
    }
}
