# OOP Example

In this example, we have an abstract Person class, which is the superclass. It has common properties like name and age, along with getters and setters for encapsulation. It also has an abstract method displayInfo() to demonstrate polymorphism.

The Student and Teacher classes are subclasses of Person, and they override the displayInfo() method according to their specific attributes. The Student class has an additional rollNumber property, while the Teacher class has an additional subject property.

In the main method, we demonstrate polymorphism by using a Person reference to refer to objects of both Student and Teacher. This allows us to call the displayInfo() method without knowing the actual class type. We also showcase encapsulation by using setters and getters to access and modify the properties of the Person object.

This example highlights the OOP concepts of abstraction, encapsulation, inheritance, and polymorphism in Java.

